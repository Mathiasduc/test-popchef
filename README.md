# Test Popchef

## First install

### Server

Ensure you have docker installed, then

``` bash
cd server
# will download a postrges image
# (or `npm run`)
yarn installDb

# start the image
yarn startDb

# will restore the world.sql dump
yarn restoreDb

yarn start:watch
```

### Client

``` bash
cd client

yarn start
```