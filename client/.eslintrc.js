module.exports = {
    extends: 'airbnb',
    env: {
        browser: true,
    },
    globals: {
        window: true,
        document: true,
    },
    rules: {
        'import/no-extraneous-dependencies': ['error', { 'devDependencies': true }],
        'max-len': ['error', 100],
        'object-curly-newline': ['error', { consistent: true }],
        'no-underscore-dangle': [2, { allow: ['_id'], allowAfterThis: true }],
        'indent': ['error', 2], //2 spaces
        // TODO: Setup a better logging and reset this to 'error'
        'no-console': ['warn'],
        'import/prefer-default-export': ['off'], //TODO: Why do we use mainly named export?
        'react/jsx-filename-extension': [1, { 'extensions': ['.js', '.jsx'] }],
        'react/jsx-indent': ['error', 2], //2 spaces
        'react/jsx-indent-props': ['error', 2], //2 spaces
        'react/jsx-boolean-value': 'never',
        'react/no-multi-comp': ['error', { ignoreStateless: true }],
        'react/destructuring-assignment': ['error', 'always', { ignoreClassFields: true }]
    }
}
