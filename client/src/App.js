import React, { Component } from 'react';
import axios from 'axios';
import { Loader, Container, Message, Header } from 'semantic-ui-react';
import ReactTable from 'react-table';

class App extends Component {
  constructor() {
    super();
    this.state = {
      cities: [],
      isFetching: false,
      fetchError: null,
    };
  }

  componentDidMount() {
    this.setState({
      isFetching: true,
    });
    axios.get('/sql/cities')
      .then((response) => {
        this.setState({
          cities: response.data,
          isFetching: false,
        });
      })
      .catch((error) => {
        console.error(error);
        this.setState({
          isFetching: false,
          fetchError: error,
        });
      });
  }

  render() {
    const {
      isFetching,
      fetchError,
      cities,
    } = this.state;

    if (isFetching === true) {
      return (<Loader active />);
    }
    if (isFetching === false && fetchError !== null) {
      const errorMessage = fetchError.response && fetchError.response.data;
      const errorStatus = fetchError.response && fetchError.response.status;
      return (
        <Message negative>
          <Message.Header>Sorry, there was a server error, please retry later.</Message.Header>
          {errorStatus && <p>{errorStatus}</p>}
          {errorMessage && <p>{errorMessage}</p>}
        </Message>);
    }

    return (
      <Container>
        <Header
          size="huge"
          textAlign="center"
        >
          {'World cities'}
        </Header>
        <ReactTable
          data={cities}
          columns={[
            {
              Header: 'Name',
              accessor: 'name',
            },
            {
              Header: 'Country ISO',
              accessor: 'countrycode',
            },
            {
              Header: 'District',
              accessor: 'district',
            },
            {
              Header: 'Population',
              accessor: 'population',
            },
          ]}
          defaultPageSize={50}
          className="-striped -highlight"
        />
      </Container>
    );
  }
}

export default App;
