const Koa = require('koa');
const { AppError } = require('./errors');
const appRoutes = require('./routes');

const app = new Koa();

// Error handler middleware
app.use(async (ctx, next) => {
  try {
    await next();
  } catch (error) {
    console.error(error);
    if (error instanceof AppError) {
      // Koa will use the error properties to send a proper error with status code and message
      throw error;
    }
    ctx.status = error.status || 500;
  }
});

app.use(appRoutes.routes());
app.use(appRoutes.allowedMethods());

process.on('unhandledRejection', (reason, promise) => {
  console.error(reason, promise);
});

process.on('uncaughtException', (err) => {
  console.error(err);
  process.exit(1);
});

app.listen(3001);
console.log('Litsening on port 3001 🚀');
