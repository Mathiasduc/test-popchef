const Router = require('koa-router');
const sqlRoutes = require('./sql');

const routes = new Router();
routes.use('/sql', sqlRoutes.routes());

module.exports = routes;
