const Router = require('koa-router');
const { readFileSync } = require('fs');
// const { AppError } = require('../../errors');
const pgClienT = require('../../pgClient');

const router = new Router();
const pathToQueries = `${__dirname}/../../assets/sql`;

const getAllCitiesQuery = readFileSync(`${pathToQueries}/getAllCities.sql`, { encoding: 'utf8' });
router.get('/', async (ctx) => {
  const result = await pgClienT.query(getAllCitiesQuery);
  // uncomment to throw an error
  // throw new AppError('Fake error that wont leak to user since the status is 500', 500);
  // throw new AppError('Fake error that will leak to the user since the status is < 500', 401);
  ctx.body = result.rows;
});

module.exports = router;
