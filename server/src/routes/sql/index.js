const Router = require('koa-router');
const requete1Routes = require('./cities');

const sqlRouter = new Router();
sqlRouter.use('/cities', requete1Routes.routes());

module.exports = sqlRouter;
