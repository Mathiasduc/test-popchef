class AppError extends Error {
  constructor(message, status, expose) {
    super();
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Something went wrong. Please try again.';
    this.status = status || 500;
    this.expose = expose !== undefined ? expose : this.status < 500;
  }
}
exports.AppError = AppError;

class NotFoundError extends AppError {
  constructor(message) {
    super(message || 'Not found', 404);
  }
}
exports.NotFoundError = NotFoundError;
