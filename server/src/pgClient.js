const { Client } = require('pg');

// Client will use environment variables
// for connection information

const client = new Client({
  user: 'popChef',
  host: 'localhost',
  database: 'postgres',
  password: 'gottaGoFast',
  port: 8081,
});
client.connect();

module.exports = client;
