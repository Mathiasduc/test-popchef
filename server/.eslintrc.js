module.exports = {
    extends: 'airbnb-base',
    env: {
        node: true,
        jest: true,
    },
    rules: {
        'max-len': ['error', 100],
        'object-curly-newline': ['error', { consistent: true }],
    }
}
